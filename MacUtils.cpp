#include "MacUtils.h"

#include <QFile>
#include <QTextStream>



MAC_INFO MacUtils::getMacInfo(QString macAddress){
    MAC_INFO result;

    QStringList user_mac= macAddress.toUpper().split(QRegExp("[:-\\s]"));
    if (user_mac.size()<3) return result;
    if (user_mac.at(0).length()==0 or user_mac.at(1).length()==0 or user_mac.at(2).length()==0) return result;

    QString oct1 = (user_mac.at(0).length()==1) ? "0"+user_mac.at(0):user_mac.at(0);
    QString oct2 = (user_mac.at(1).length()==1) ? "0"+user_mac.at(1):user_mac.at(1);
    QString oct3 = (user_mac.at(2).length()==1) ? "0"+user_mac.at(2):user_mac.at(2);


    QString find_mac = oct1+""+oct2+""+oct3;
    result.mac_address=oct1+"-"+oct2+"-"+oct3+"-XX-XX-XX";

    QString path_oui = "oui.csv";


#if defined(PATH_OUI)
    path_oui = QString(PATH_OUI);
#else
    QFile tryoui ("/usr/share/macaddressview/oui.csv");
    if (tryoui.exists()){
        path_oui=tryoui.fileName();
    }
#endif

    QFile file(path_oui);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
            QStringList pMac = line.split(",");
            if (find_mac==pMac.at(1)){
                QStringList fields =  parseCSVLine(line);
                result.name_org=fields.at(2);
                result.address_org=fields.at(3);
                break;
            }
        }
    }
    file.close();


    return result;
}



QStringList MacUtils::parseCSVLine(QString line){
    enum State {Normal, FindStart, Quote} state = Normal;
    QStringList result;
    QString value;

    QChar separator = ',';


    for (int i=0;i<line.size();i++){
        QChar current_char = line.at(i);

        if (state == FindStart){
            if (current_char==separator){
                state = Normal;
                continue;
            }
        }

        if ( state == Normal){
            if (current_char==separator){
                result.push_back(value);
                value.clear();
                continue;
            }
            if (current_char=='"'){
                state=Quote;
                continue;
            }

            value.append(current_char);
        }

        if ( state == Quote){
            if (current_char=='"'){
                result.push_back(value);
                value.clear();
                state=FindStart;
                continue;
            }

            value.append(current_char);
        }
    }
    if (state==Normal) result.append(value);

    return result;
}
