#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QCommandLineParser>
#include <QDebug>

#include "version.h"
#include "MacUtils.h"
#include "mainwindow.h"

#include "iostream"

int main (int argc, char * argv[]){

    QString locale = QLocale::system().name();

    QTranslator qtTranslator;
    qtTranslator.load("qt_"+locale,QLibraryInfo::location(QLibraryInfo::TranslationsPath));

    QTranslator translator;
    if (translator.load(QString("macaddressview_") + locale)==false){
        translator.load(QString("/usr/share/macaddressview/langs/macaddressview_") + locale);
    }

    QString argMacAddress;

    {
        QCoreApplication app(argc, argv);
        QCoreApplication::setApplicationName("MACAddressView");
        QCoreApplication::setApplicationVersion(VERSION);
        QCoreApplication::setOrganizationDomain("dansoft.ru");

        app.installTranslator(&qtTranslator);
        app.installTranslator(&translator);

        QCommandLineParser parser;
        parser.setApplicationDescription(QCoreApplication::tr("A tool to view information according to a given MAC address"));
        parser.addHelpOption();
        parser.addVersionOption();

        QCommandLineOption optionMacAddress(QStringList() << "mac",
            QCoreApplication::tr("mac address on which you want to get information"),
            QCoreApplication::tr("mac"));
        parser.addOption(optionMacAddress);


        parser.process(app);

        argMacAddress = parser.value(optionMacAddress);

        if (!argMacAddress.isEmpty()){
            MAC_INFO macInfo =  MacUtils::getMacInfo(argMacAddress);
            std::cout << QCoreApplication::tr("MAC address:").toStdString()+" " << macInfo.mac_address.toStdString() << std::endl;
            std::cout << QCoreApplication::tr("Organization name:").toStdString()+" " << macInfo.name_org.toStdString() << std::endl;
            std::cout << QCoreApplication::tr("Organization address:").toStdString()+" " << macInfo.address_org.toStdString() << std::endl;
            return 0;
        }
    }




    QApplication app(argc, argv);

    app.installTranslator(&qtTranslator);
    app.installTranslator(&translator);

    MainWindow form;
    form.show();

    return app.exec();
}

