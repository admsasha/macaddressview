#-------------------------------------------------
#
# Project created by QtCreator 2018-06-29T09:33:34
#
#-------------------------------------------------

TEMPLATE = app
TARGET = macaddressview
DEPENDPATH += .
INCLUDEPATH += .
DESTDIR = Bin

ARCH = $$QMAKE_HOST.arch

CONFIG(debug, release|debug){
    MOC_DIR = .build/$$ARCH/debug
    OBJECTS_DIR = .build/$$ARCH/debug
    UI_DIR = .build/$$ARCH/debug
    RCC_DIR = .build/$$ARCH/debug
}
CONFIG(release, release|debug){
    MOC_DIR = .build/$$ARCH/release
    OBJECTS_DIR = .build/$$ARCH/release
    UI_DIR = .build/$$ARCH/release
    RCC_DIR = .build/$$ARCH/release
}


QT += widgets

QMAKE_CXXFLAGS += -std=gnu++11
QMAKE_CXXFLAGS += -pedantic -pedantic-errors
QMAKE_CXXFLAGS += -Wall -Wextra -Wformat -Wformat-security -Wno-unused-variable -Wno-unused-parameter
QMAKE_CXXFLAGS += -Wno-unknown-pragmas

RESOURCES += MACAddressView.qrc
win32: RC_FILE = MACAddressView.rc


TRANSLATIONS = $$files(langs/macaddressview_*.ts)

### install ###

isEmpty(QMAKE_LRELEASE) {
    win32|os2:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]\lrelease.exe
    else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
    unix {
        !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease-qt5 }
    } else {
        !exists($$QMAKE_LRELEASE) { QMAKE_LRELEASE = lrelease }
    }
}

!win32 {
  system($${QMAKE_LRELEASE} -silent $${_PRO_FILE_} 2> /dev/null)
}
win32 {
  system($${QMAKE_LRELEASE} $${_PRO_FILE_})
}

updateqm.input = TRANSLATIONS
updateqm.output = langs/${QMAKE_FILE_BASE}.qm
updateqm.commands = $$QMAKE_LRELEASE -silent ${QMAKE_FILE_IN} -qm langs/${QMAKE_FILE_BASE}.qm
updateqm.CONFIG += no_link target_predeps
QMAKE_EXTRA_COMPILERS += updateqm

data_bin.path = /usr/bin/
data_bin.files = Bin/macaddressview
INSTALLS += data_bin

data_app.path = /usr/share/applications/
data_app.files = pkg/macaddressview.desktop
INSTALLS += data_app

data_pixmaps.path = /usr/share/pixmaps/
data_pixmaps.files = data/macaddressview.png
INSTALLS += data_pixmaps

data_oui.path = /usr/share/macaddressview/
data_oui.files = data/oui.csv
INSTALLS += data_oui

data_langs.path = /usr/share/macaddressview/langs/
data_langs.files = langs/*.qm
INSTALLS += data_langs

# Input
SOURCES += main.cpp \
    MacUtils.cpp \
    mainwindow.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    MacUtils.h \
    mainwindow.h \
    version.h
