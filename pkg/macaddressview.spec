Summary:	MAC info viewer
Name:		macaddressview
Version:	1.0.0
Release:	1
License:	GPLv2+
Group:		Networking/Other
Url:		https://bitbucket.org/admsasha/%{name}
Source0:	https://bitbucket.org/admsasha/%{name}/downloads/%{name}-%{version}.tar.gz
BuildRequires:	qt5-linguist-tools
BuildRequires:	pkgconfig(Qt5Core)
BuildRequires:	pkgconfig(Qt5Gui)
BuildRequires:	pkgconfig(Qt5Widgets)

%description
A tool to view information according to a given MAC address.

%prep
%setup -q

%build
%qmake_qt5
%make

%install
%make_install INSTALL_ROOT=%{buildroot}


%files
%doc README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/*
%{_datadir}/pixmaps/%{name}.png
