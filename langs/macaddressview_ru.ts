<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="27"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="40"/>
        <source>MAC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="133"/>
        <source>Organization name:</source>
        <translation>Имя организации:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="69"/>
        <source>Organization address:</source>
        <translation>Адрес организации:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="162"/>
        <source>Mac address:</source>
        <translation>MAC-адрес:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="104"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="117"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>A tool to view information according to a given MAC address.</source>
        <translation>Инструмент для просмотра информации в соответствии с заданным MAC-адресом.</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="37"/>
        <source>A tool to view information according to a given MAC address</source>
        <translation>Инструмент для просмотра информации в соответствии с заданным MAC-адресом.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="42"/>
        <source>mac address on which you want to get information</source>
        <translation>MAC адрес по которому хотите получить информацию</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="43"/>
        <source>mac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="53"/>
        <source>MAC address:</source>
        <translation>MAC-адрес:</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="54"/>
        <source>Organization name:</source>
        <translation>Имя организации:</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>Organization address:</source>
        <translation>Адрес организации:</translation>
    </message>
</context>
</TS>
