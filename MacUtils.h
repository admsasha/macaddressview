#ifndef MACUTILS_H
#define MACUTILS_H

#include <QStringList>

struct MAC_INFO {
    QString mac_address;
    QString name_org;
    QString address_org;
};

class MacUtils {
    public:
        static MAC_INFO getMacInfo(QString macAddress);

        static QStringList parseCSVLine(QString line);
};

#endif // MACUTILS_H
