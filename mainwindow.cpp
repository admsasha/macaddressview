#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QDebug>

#include "version.h"
#include "MacUtils.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    this->setFixedSize(this->width(),this->height());
    this->setWindowTitle(QString("MACAddressView %1 (%2)").arg(VERSION).arg(DATE_BUILD));
    this->setWindowIcon(QIcon(":/data/MACAddressView.ico"));

    connect(ui->pushButton,SIGNAL(clicked(bool)),SLOT(close()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),SLOT(showAboutQt()));
    connect(ui->pushButton_3,SIGNAL(clicked(bool)),SLOT(showAbout()));
    connect(ui->lineEdit,SIGNAL(textEdited(QString)),this,SLOT(findMac()));

    findMac();
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::findMac(){
    MAC_INFO macInfo =  MacUtils::getMacInfo(ui->lineEdit->text());
    ui->label_7->setText(macInfo.mac_address);
    ui->label_3->setText(macInfo.name_org);
    ui->label_5->setText(macInfo.address_org);
}

void MainWindow::showAboutQt(){
    QMessageBox::aboutQt(this);
}

void MainWindow::showAbout(){
    QString title=this->windowTitle();
    QString text=tr("A tool to view information according to a given MAC address.")+"<BR><BR>";
    text.append("www: <a href=\"http://dansoft.ru\">http://dansoft.ru</a><br>");
    text.append("email: <a href=\"mailto://dik@inbox.ru\">dik@inbox.ru</a>");

    QMessageBox::about(this,title,text);
}
